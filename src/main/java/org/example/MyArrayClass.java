package org.example;

public class MyArrayClass {
    private Object[] objects;
    private int size = 0;

    public MyArrayClass(int count){
        this.objects = new Object[count];
    }

    public int size(){
        return size;
    }

    public boolean isEmpty(){

        for (Object object : objects){
            if(object != null){
                return true;
            }
        }

        return false;

    }

    public Object get(int index){
        if (index >= size){
            return null;
        }

        return objects[index];
    }

    public void add(Object object){
        if(size >= 16){
            return;
        }

        objects[size] = object;
        size += 1;
    }

    public void remove(int index){
        objects[index] = null;
        size -= 1;
    }

    public void remove(Object object){
        for (int i = 0; i < size; i++){
            if(objects[i] == object){
                objects[i] = null;
                size -= 1;
            }
        }
    }

    public boolean contains(Object object){
        for (Object obj : objects){
            if(obj == object){
                return true;
            }
        }
        return false;
    }

    public void clear(){
        for (int i = 0; i < size; i++){
            objects[i] = null;
        }
    }
}
